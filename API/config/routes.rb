Rails.application.routes.draw do
  resources :enterprises
  mount_devise_token_auth_for 'User', at: 'auth'

  namespace :api do
    scope :v1 do
      scope :users do
      mount_devise_token_auth_for 'User', at: 'auth'
    end
  end
  end


  get 'api/v1/enterprises', to:"enterprises#index"

  get 'api/v1/enterprises/:id', to:"enterprises#show"

  #get 'enterprises/index', controller:'enterprises', actin:'index',as:'api/v1/enterprises'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
