class CreateEnterprises < ActiveRecord::Migration[5.1]
  def change
    create_table :enterprises do |t|
      t.string :email_enterprise
      t.string :facebook
      t.string :twitter
      t.string :linkedin
      t.string :phone
      t.string :own_enterprise
      t.string :enterprise_name
      t.blob :photo
      t.text :description
      t.string :city
      t.string :country
      t.integer :value
      t.integer :share_price


    end
  end
end
