class EnterprisesController < ApplicationController
  before_action  :set_enterprise, only: [:show, :update, :destroy]
  #exige o login para todos os metodos do controller
  before_action  :authenticate_user!


  def index
    objenterprise=Enterprise.new
    if(params[:name].nil?&&params[:enterprise_types].nil?)

      @enterprises=Enterprise.all
      render json:@enterprises
  else
    #retorno com filtro de tipo de empresa e nome
      @enterprises=objenterprise.findbyfilter(params[:enterprise_types],params[:name])
    render json: @enterprises

  end
end


  def show
    render json: @enterprise
  end


  def create
    @enterprise = Enterprise.new(enterprise_params)

    if @enterprise.save
      render json: @enterprise, status: :created, location: @enterprise
    else
      render json: @enterprise.errors, status: :unprocessable_entity
    end
  end


  def update
    if @enterprise.update(enterprise_params)
      render json: @enterprise
    else
      render json: @enterprise.errors, status: :unprocessable_entity
    end
  end


  def destroy
    @enterprise.destroy
  end

  private

    def set_enterprise
      @enterprise = Enterprise.find(params[:id])
    end


    def enterprise_params
      params.require(:enterprise).permit(:email_enterprise, :facebook, :twitter, :linkedin, :phone, :own_enterprise, :enterprise_name, :photo, :description, :city, :country, :value, :share_price,:enterprise_type_id)
    end
end
