require 'test_helper'

class EnterprisesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @enterprise = enterprises(:one)
  end

  test "should get index" do
    get enterprises_url, as: :json
    assert_response :success
  end

  test "should create enterprise" do
    assert_difference('Enterprise.count') do
      post enterprises_url, params: { enterprise: { city: @enterprise.city, country: @enterprise.country, description: @enterprise.description, email_enterprise: @enterprise.email_enterprise, enterprise_name: @enterprise.enterprise_name, facebook: @enterprise.facebook, linkedin: @enterprise.linkedin, own_enterprise: @enterprise.own_enterprise, phone: @enterprise.phone, photo: @enterprise.photo, share_price: @enterprise.share_price, twitter: @enterprise.twitter, value: @enterprise.value } }, as: :json
    end

    assert_response 201
  end

  test "should show enterprise" do
    get enterprise_url(@enterprise), as: :json
    assert_response :success
  end

  test "should update enterprise" do
    patch enterprise_url(@enterprise), params: { enterprise: { city: @enterprise.city, country: @enterprise.country, description: @enterprise.description, email_enterprise: @enterprise.email_enterprise, enterprise_name: @enterprise.enterprise_name, facebook: @enterprise.facebook, linkedin: @enterprise.linkedin, own_enterprise: @enterprise.own_enterprise, phone: @enterprise.phone, photo: @enterprise.photo, share_price: @enterprise.share_price, twitter: @enterprise.twitter, value: @enterprise.value } }, as: :json
    assert_response 200
  end

  test "should destroy enterprise" do
    assert_difference('Enterprise.count', -1) do
      delete enterprise_url(@enterprise), as: :json
    end

    assert_response 204
  end
end
