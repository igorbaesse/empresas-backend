require 'test_helper'

class EnterpriseTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @enterprise_type = enterprise_types(:one)
  end

  test "should get index" do
    get enterprise_types_url, as: :json
    assert_response :success
  end

  test "should create enterprise_type" do
    assert_difference('EnterpriseType.count') do
      post enterprise_types_url, params: { enterprise_type: { enterprise_type_name: @enterprise_type.enterprise_type_name } }, as: :json
    end

    assert_response 201
  end

  test "should show enterprise_type" do
    get enterprise_type_url(@enterprise_type), as: :json
    assert_response :success
  end

  test "should update enterprise_type" do
    patch enterprise_type_url(@enterprise_type), params: { enterprise_type: { enterprise_type_name: @enterprise_type.enterprise_type_name } }, as: :json
    assert_response 200
  end

  test "should destroy enterprise_type" do
    assert_difference('EnterpriseType.count', -1) do
      delete enterprise_type_url(@enterprise_type), as: :json
    end

    assert_response 204
  end
end
